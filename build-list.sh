#!/bin/env bash

KEEP_GROUPS=( base base-devel xorg-apps )
CUSTOM_REPOS=( aur aur-git )

# helper function for building pacman config without custom AUR repos
# @see https://github.com/AladW/aurutils/commit/88321e82dfd7ebd68fcc2c2ce415443eda86ba8d
build_config() {
    printf '%s\n' '[options]'
    pacconf --raw --options

    for i in core testing extra community community-testing multilib multilib-testing; do
        if pacconf --repo="$i" >/dev/null 2>&1; then
            printf '%s\n' "[$i]"
            pacconf --raw --repo="$i"
        fi
    done
}

function join_by { local IFS="$1"; shift; echo "$*"; }

AWK_FILTER='/^Name/ { name=$3 } /^Groups/ { for (i = 3; i <= NF; i++) if ( $i ~ /^('$(join_by "|" "${KEEP_GROUPS[@]}")')$/ ) { print $i; next } { print name } }'

config=$(build_config)

# ---- default repo packages ----
# explicitly installed
pacman --config=<(echo "$config") -Qnei | awk "$AWK_FILTER" | sort | uniq > installed
# optional dependencies (reinstall with --asdeps)
comm -13 <(pacman --config=<(echo "$config") -Qnqdt | sort) <(pacman --config=<(echo "$config") -Qnqdtt | sort) > installed.optdeps


# ---- custom repo packages ----
for repo in "${CUSTOM_REPOS[@]}"
do
    # explicitly installed
    comm -12 <(pacman -Qnei | awk "$AWK_FILTER" | sort | uniq) <(paclist "$repo" | cut -f 1 -d " " | sort) > "installed.$repo"
    # optional dependencies (reinstall with --asdeps)
    comm -12 <(comm -13 <(pacman -Qnqdt | sort) <(pacman -Qnqdtt | sort)) <(paclist "$repo" | cut -f 1 -d " " | sort) > "installed.optdeps.$repo"
done

